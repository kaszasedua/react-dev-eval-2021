import React from 'react';

// class containing information about the canvas that is used in
// multiple components
export default class CanvasUtils {
  constructor(canvasRef) {
    this.canvas = canvasRef;
    this.ctx = canvasRef.getContext('2d');
  }

  getPositionFromMouse = (mousePos) => {
    const bounding = this.canvas.getBoundingClientRect();
    const width = this.canvas.getAttribute('width');
    const height = this.canvas.getAttribute('height');
    const x = ((mousePos.current.x - bounding.left) / bounding.width) * width;
    const y = ((mousePos.current.y - bounding.top) / bounding.height) * height;

    return { x, y };
  };
}
