export const theme = {
  palette: {
    primary: '#f28482',
    secondary: '#f6bd60',
    background: '#f6e1d5',
    text: '#030303',
    button: '#fff',
  },
  typography: {
    title: {
      fontFamily: 'Khula, sans-serif',
    },
    body: {
      fontFamily: 'Poppins, sans-serif',
    },
  },
};
