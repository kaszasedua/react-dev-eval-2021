import { useCallback, useEffect, useRef, useState } from 'react';

let animationFrame = 0;

export const useWebcamCapture = (
  stickerImg,
  title,
  clearSelection,
  isPainting,
  paint,
  canvasRef,
  mousePos,
  canvasUtils
) => {
  const [videoRef, setVideoRef] = useState();
  const [picture, setPicture] = useState();
  const [confirmedStickers, setConfirmedStickers] = useState([]);

  const onVideoRef = useCallback((node) => {
    setVideoRef(node);
  });

  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (videoRef && canvasRef && !initialized) {
      navigator.mediaDevices
        .getUserMedia({
          video: {
            width: { min: 1024, ideal: 1280, max: 1920 },
            height: { min: 576, ideal: 720, max: 1080 },
          },
          audio: false,
        })
        .then(function (stream) {
          videoRef.srcObject = stream;
          videoRef.play();
        })
        .catch(function (err) {
          console.log("Couldn't start webcam: " + err);
        });

      const onCanPlay = function (ev) {
        const width = videoRef.videoWidth;
        const height = videoRef.videoHeight / (videoRef.videoWidth / width);

        videoRef.setAttribute('width', width);
        videoRef.setAttribute('height', height);
        canvasRef.setAttribute('width', width);
        canvasRef.setAttribute('height', height);
        videoRef.removeEventListener('canplay', onCanPlay, false);
      };

      videoRef.addEventListener('canplay', onCanPlay, false);
      setInitialized(true);
    } else if (!videoRef || !canvasRef) {
      setInitialized(false);
    }
  }, [videoRef, canvasRef, initialized]);

  const startRenderLoop = useCallback(() => {
    if (canvasRef && videoRef && canvasUtils) {
      // cancel previous loop before starting a new one
      window.cancelAnimationFrame(animationFrame);

      const renderFrame = () => {
        const ctx = canvasRef.getContext('2d');
        const width = canvasRef.getAttribute('width');
        const height = canvasRef.getAttribute('height');
        ctx.drawImage(videoRef, 0, 0, width, height);

        if (stickerImg) {
          const { x, y } = canvasUtils.getPositionFromMouse(mousePos);

          ctx.drawImage(
            stickerImg,
            x - width * 0.2,
            y - width * 0.2,
            width * 0.25,
            width * 0.25
          );
        }

        if (confirmedStickers.length > 0 && ctx)
          confirmedStickers.forEach((sticker) => {
            ctx.drawImage(
              sticker.img,
              sticker.x - width * 0.2,
              sticker.y - width * 0.2,
              width * 0.25,
              width * 0.25
            );
          });

        if (isPainting) paint();
        // using .stroke here because stroke needs to be run every frame,
        // otherwise the painting disappears
        ctx.stroke();

        //@TODO ask about this if I happen to pass to the next round
        animationFrame = requestAnimationFrame(renderFrame);
      };

      animationFrame = requestAnimationFrame(renderFrame);
    }
  }, [
    canvasRef,
    videoRef,
    stickerImg,
    confirmedStickers,
    isPainting,
    mousePos,
    canvasUtils,
    paint,
  ]);

  useEffect(() => {
    startRenderLoop();
  }, [startRenderLoop]);

  // Draw stickers on canvas
  useEffect(() => {
    if (canvasRef && stickerImg && canvasUtils) {
      const onMouseDown = (ev) => {
        const { x, y } = canvasUtils.getPositionFromMouse(mousePos);

        setConfirmedStickers((confirmedStickers) => [
          ...confirmedStickers,
          { img: stickerImg, x: x, y: y },
        ]);

        // Clear selection so a new sticker can be selected
        // originally had a setTimeout around it
        clearSelection();
      };

      const onMouseMove = (ev) => {
        mousePos.current = { x: ev.clientX, y: ev.clientY };
      };

      canvasRef.addEventListener('mousemove', onMouseMove);
      canvasRef.addEventListener('mousedown', onMouseDown);

      return () => {
        canvasRef.removeEventListener('mousemove', onMouseMove);
        canvasRef.removeEventListener('mousedown', onMouseDown);
      };
    }
  }, [
    canvasRef,
    stickerImg,
    clearSelection,
    confirmedStickers,
    mousePos,
    canvasUtils,
  ]);

  // Capture image
  const onCapture = useCallback(
    (ev) => {
      if (canvasRef) {
        const data = canvasRef.toDataURL('image/png');
        setPicture({ dataUri: data, title });
      }
    },
    [canvasRef, title]
  );

  return [onVideoRef, onCapture, picture];
};
