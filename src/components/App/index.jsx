import { Route, Routes } from 'react-router-dom';

import Editor from '../Editor';
import Home from '../Home';
import useStyles from './useStyles';

function App() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/editor' element={<Editor />} />
      </Routes>
    </div>
  );
}

export default App;
