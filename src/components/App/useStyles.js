import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  '@global body': {
    margin: 0,
    padding: 0,
    height: '100%',
    width: '100%',
    fontSize: '16px',
    fontFamily: theme.typography.body.fontFamily,
    color: theme.palette.text,
    background: theme.palette.background,
  },

  container: {
    display: 'flex',
    justifyContent: 'center',
    minHeight: '100vh',
    '& a': {
      color: theme.palette.text,
    },
  },
}));
