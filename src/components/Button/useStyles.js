import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  button: {
    margin: '1em',
    padding: '.5em 1em',
    borderRadius: 20,
    fontSize: '1.1rem',
    color: theme.palette.primary,
    fontFamily: theme.typography.body.fontFamily,
    backgroundColor: theme.palette.background,
    border: '2px solid',
    borderColor: theme.palette.primary,
    transition: 'color .2s ease-in-out',
    cursor: 'pointer',

    '&:hover': {
      color: theme.palette.background,
      backgroundColor: theme.palette.primary,
      textDecoration: 'none',
    },
  },
}));
