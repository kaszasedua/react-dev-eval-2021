import React from 'react';

import useStyles from './useStyles';

export default function StartButton(props) {
  const classes = useStyles();

  return (
    <>
      <button className={classes.button} onClick={props.onClick}>
        {props.label}
      </button>
    </>
  );
}
