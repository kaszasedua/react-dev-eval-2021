import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    margin: '1em',
    display: 'flex',
    justifyContent: 'center',

    '& input': {
      textAlign: 'center',
      background: 'transparent',
      border: 'none',
      color: theme.palette.primary,
      fontSize: '1.5em',
      fontFamily: theme.typography.body.fontFamily,

      '&:focus': {
        outline: 'none',
      },
    },
  },
}));
