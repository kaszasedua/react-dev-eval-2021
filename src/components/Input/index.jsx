import React from 'react';

import useStyles from './useStyles';

export default function Input(props) {
  const classes = useStyles();

  function handleChange(e) {
    const value = e.target.value;

    props.onChange(value);
  }

  return (
    <div className={classes.container}>
      <input
        type='text'
        value={props.title}
        onChange={handleChange}
        placeholder={props.placeholder}
      />
    </div>
  );
}
