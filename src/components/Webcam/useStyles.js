import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    margin: '1em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& canvas': {
      height: 'auto',
      width: '70%',
      borderRadius: 10,
    },

    '& video': {
      display: 'none',
    },
  },
}));
