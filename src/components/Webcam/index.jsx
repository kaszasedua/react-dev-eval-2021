import React, { useState } from 'react';

import useStyles from './useStyles';

export default function Webcam(props) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <video ref={props.handleVideoRef} className={classes.video} />
      <canvas
        className={classes.canvas}
        ref={props.handleCanvasRef}
        width={2}
        height={2}
      />
    </div>
  );
}
