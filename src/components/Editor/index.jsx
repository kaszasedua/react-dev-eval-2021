import React, { useState, useEffect, useCallback, useRef } from 'react';

import Button from '../Button';
import Input from '../Input';
import CapturedPicture from '../CapturedPicture';
import EditorTools from '../EditorTools';
import { useWebcamCapture } from '../../hooks/useWebcamCapture';
import Webcam from '../Webcam';
import CanvasUtils from '../../utils/CanvasUtils';
import useStyles from './useStyles';

let canvasUtils = null;

export default function Editor() {
  const classes = useStyles();

  const [sticker, setSticker] = useState();
  const [title, setTitle] = useState('');
  const [previewToggled, setPreviewToggled] = useState(false);
  const [isPainting, setIsPainting] = useState(false);
  const [hasStartedStroke, setHasStartedStroke] = useState(false);
  const [paintColor, setPaintColor] = useState('#f28482');
  const [canvasRef, setCanvasRef] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const mousePos = useRef({ x: 0, y: 0 });
  const editorRef = useRef();

  const [handleVideoRef, handleCapture, picture] = useWebcamCapture(
    sticker?.img,
    title,
    clearSelection,
    isPainting,
    paint,
    canvasRef,
    mousePos,
    canvasUtils
  );

  const onCanvasRef = useCallback((node) => {
    if (!canvasUtils) canvasUtils = new CanvasUtils(node);

    setCanvasRef(node);
  });

  // @TODO need to find a way to identify when the canvas element is loaded as that loads way later than
  // the rest of the elements on the same page. The workaround for now is to load in all contents of the components
  // after a time delay, however, this is only looks nice if you don't actually check the code
  const onLoading = () => {
    editorRef.current.className = classes.loading;

    setTimeout(() => {
      editorRef.current.className = classes.container;

      setIsLoading(false);
    }, 1500);
  };

  useEffect(onLoading, []);

  useEffect(() => {
    if (!canvasRef || sticker) return;

    canvasUtils.ctx.strokeStyle = paintColor;
    canvasUtils.ctx.lineCap = 'round';
    canvasUtils.ctx.lineJoin = 'round';
    canvasUtils.ctx.lineWidth = 2.5;

    if (isPainting) {
      const onMouseDown = (ev) => {
        if (hasStartedStroke) {
          finishPainting();
        } else {
          mousePos.current = { x: ev.clientX, y: ev.clientY };
          startStroke();
        }
      };

      const onMouseMove = (ev) => {
        if (hasStartedStroke)
          mousePos.current = { x: ev.clientX, y: ev.clientY };
      };

      canvasRef.addEventListener('mousemove', onMouseMove);
      canvasRef.addEventListener('mousedown', onMouseDown);
      return () => {
        canvasRef.removeEventListener('mousemove', onMouseMove);
        canvasRef.removeEventListener('mousedown', onMouseDown);
      };
    }
  }, [
    canvasRef,
    isPainting,
    paintColor,
    finishPainting,
    paint,
    hasStartedStroke,
  ]);

  // while painting, I need to select a starting point by clicking
  // on the canvas once, which is why startStroke is needed
  function startStroke(ev) {
    if (!canvasRef) return;

    const { x, y } = canvasUtils.getPositionFromMouse(mousePos);

    canvasUtils.ctx.beginPath();
    canvasUtils.ctx.moveTo(x, y);

    setHasStartedStroke(true);
  }

  function finishPainting() {
    if (!canvasRef) return;

    setIsPainting(false);
    setHasStartedStroke(false);
  }

  //@TODO need to figure out a way to be able to paint more than one "line"
  // currently, the moment the canvas is clicked after painting, you can't start painting again unless
  // you click the paintbrush once more, however, in that case you restart the process
  function paint() {
    if (!canvasRef) return;

    const { x, y } = canvasUtils.getPositionFromMouse(mousePos);

    canvasUtils.ctx.lineTo(x, y);
  }

  // Function to stop sticker from being selected
  function clearSelection() {
    setSticker(null);
  }

  function handleClick() {
    setPreviewToggled((previewToggled) => !previewToggled);
    handleCapture();
  }

  return (
    <div className={classes.container}>
      {!previewToggled ? (
        <>
          {isLoading && (
            <p className={classes.loadingString}>
              Wait a moment, I'm loading in some cool stuff...
            </p>
          )}
          <div ref={editorRef}>
            <Input
              title={title}
              onChange={setTitle}
              placeholder='Give it a title!'
            />
            <Webcam
              handleVideoRef={handleVideoRef}
              handleCanvasRef={onCanvasRef}
              handleCapture={handleCapture}
            />
            <EditorTools
              isPainting={isPainting}
              setIsPainting={setIsPainting}
              sticker={sticker}
              setSticker={setSticker}
            />
            <Button label='Display my detroyed face!' onClick={handleClick} />
          </div>
        </>
      ) : (
        <CapturedPicture picture={picture} />
      )}
    </div>
  );
}
