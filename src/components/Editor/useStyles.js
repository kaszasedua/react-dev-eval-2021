import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  loading: {
    display: 'none',
  },
  loadingString: {
    color: theme.palette.primary,
  },
}));
