import React, { useRef } from 'react';

import slap from '../../assets/images/slap.png';
import punchBottom from '../../assets/images/punch_bottom.png';
import punchFront from '../../assets/images/punch_front.png';
import punchSide from '../../assets/images/punch_side.png';
import useStyles from './useStyles';

export default function StickerGallery(props) {
  const classes = useStyles();

  // adding the ref for each image separately as I need unique ones
  // to be able to select each of them
  const images = [
    { img: slap, alt: 'Slap', ref: useRef(null) },
    { img: punchBottom, alt: 'Punch from bottom', ref: useRef(null) },
    { img: punchFront, alt: 'Punch from front', ref: useRef(null) },
    { img: punchSide, alt: 'Punch from front', ref: useRef(null) },
  ];

  function selectSticker(ref) {
    props.setSticker({
      img: ref.current,
    });
  }

  return (
    <div className={classes.container}>
      {images.map((image, index) => {
        const id = `sticker_selector_${index}`;

        return (
          <button onClick={() => selectSticker(image.ref)} key={id}>
            <img id={id} ref={image.ref} src={image.img} alt={image.alt} />
          </button>
        );
      })}
    </div>
  );
}
