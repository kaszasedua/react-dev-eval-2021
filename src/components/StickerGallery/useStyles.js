import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    margin: '1em',
    display: 'flex',
    justifyContent: 'start',

    '& button': {
      background: 'none',
      border: 'none',
      cursor: 'pointer',

      '& :hover': {
        transform: 'scale(1.1)',
      },
    },

    '& img': {
      height: '5em',
      width: 'auto',
      transition: 'all .2s ease-in-out',
    },
  },
}));
