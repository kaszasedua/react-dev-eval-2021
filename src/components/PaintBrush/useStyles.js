import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    margin: '1em',
    display: 'flex',
    justifyContent: 'end',
    cursor: 'pointer',
    '& :hover': {
      transform: 'scale(1.1)',
    },

    '& svg': {
      color: theme.palette.primary,
      transition: 'all .2s ease-in-out',

      '& :hover': {
        transform: 'none',
      },
    },
  },
}));
