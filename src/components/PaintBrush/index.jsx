import React from 'react';
import { faPaintbrush } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import useStyles from './useStyles';

export default function PaintBrush(props) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <FontAwesomeIcon
        icon={faPaintbrush}
        onClick={props.onClick}
        fontSize={50}
        className={classes.icon}
      />
    </div>
  );
}
