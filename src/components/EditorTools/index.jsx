import React from 'react';

import PaintBrush from '../PaintBrush';
import StickerGallery from '../StickerGallery';
import useStyles from './useStyles';

export default function EditorTools(props) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <StickerGallery sticker={props.sticker} setSticker={props.setSticker} />
      <PaintBrush onClick={() => props.setIsPainting(!props.isPainting)} />
    </div>
  );
}
