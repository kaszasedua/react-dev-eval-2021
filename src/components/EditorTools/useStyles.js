import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    margin: '1em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    gap: '3em',
    cursor: 'pointer',
  },
}));
