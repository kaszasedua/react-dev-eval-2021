import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    flexDirection: 'column',
    maxWidth: '50%',
    '&  h1': {
      margin: 0,
      background:
        'linear-gradient(90deg, rgba(242,132,130,1) 0%, rgba(246,189,96,1) 100%)',
      WebkitBackgroundClip: 'text',
      WebkitTextFillColor: 'transparent',
      fontFamily: theme.typography.title.fontFamily,
      fontSize: '4rem',
    },
  },
}));
