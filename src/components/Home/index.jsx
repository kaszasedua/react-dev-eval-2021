import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../Button';
import useStyles from './useStyles';

export default function Home() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h1>SlapSticker</h1>
      <p>
        Have you ever said something so dumb, you just wanted to slap yourself?
        Well now you can!
      </p>
      <Link to='/editor'>
        <Button label='Start' />
      </Link>
    </div>
  );
}
