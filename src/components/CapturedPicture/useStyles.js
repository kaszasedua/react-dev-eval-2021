import { createUseStyles } from 'react-jss';

export default createUseStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',

    '& img': {
      width: '50%',
      height: 'auto',
      borderWidth: '.8rem .8rem 4rem',
      borderStyle: 'solid',
      borderColor: '#fff',
      boxShadow: '5px 5px 15px 5px rgba(0,0,0,0.19)',
    },

    '& h3': {
      marginTop: '13em',
      position: 'absolute',
      fontSize: '2rem',
      fontFamily: theme.typography.title.fontFamily,
      color: theme.palette.primary,
    },
  },
}));
