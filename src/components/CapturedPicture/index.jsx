import React from 'react';

import useStyles from './useStyles';

export default function CapturedPicture(props) {
  const classes = useStyles();

  return (
    <>
      {props.picture && (
        <div className={classes.container}>
          <img src={props.picture.dataUri} alt={props.picture.title} />
          <h3>{props.picture.title}</h3>
        </div>
      )}
    </>
  );
}
